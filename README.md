## USER STORIES (BASIC REQUIREMENTS): 

1. Deciding on hosting services and buying things required 
	* What is the overhead cost in upgrading from 100 connections to n connections. What does 100 connection means.
2. Making Key HTML Structure 
3. Making mobile first design layout with changes in each screen. 
	*	Identifying images and basic design layout

4. Building Form Outlining
5. Deciding on appropriate APIS or using default APIS and styling choices
6. Build Radmap Pages - individual pages - with routing through buttons/ links  
8. Data visualization tools identification 
9. APIS for which doesn�t have vulnerability (Bar Graph, Pi Chart) 
10. Find a safe table api  
11. SEO 
 	* RND
	* And optimise 
12. Backend database storage
	* Building firebase or whatever services
13. Options for saving high Quality video and images for the project  
14. Ensuring website safety
	* SSL certificates 
	* Data safety
	

## USER STORIES (ADDITIONAL REQUIREMENTS): 
1. Fully customized data for mobile centric development.
2. Adding Comment sections for user interactivity. 
3. Building Dashboard and admin screen
	* Make a CSV file downloading API
	* Make a Table to visualize data 
	* Make future possibilities for more busienss requirements 

## Timeline 

| Day     | Work    | Remarks |
| --------|---------|---------|
| 5 Nov  | Basic Reuqirement   |   |
| 10 Nov | Advance Requirement |  |
| 15 Nov | Demo 1 |    |


## Checkin and Code
Although we will make mistakes, let's try to minimize such mistakes as we need to deliver by timeline. So few thinks to keep in mind.

To ensure that we don't erase code by mistake, let's checkin to seperate branch and raise pull request and not commit directly to the master codeline. 

Comment on code, where necessary, to ensure either person knows what the intent of the code is. 


## RESOURCES 

#### UI DESIGNING: 

1. [Angular Material UI](https://material.angular.io/)
2. [All Other Libraries](https://blog.bitsrc.io/11-angular-component-libraries-you-should-know-in-2018-e9f9c9d544ff)

