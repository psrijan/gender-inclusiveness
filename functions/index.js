const functions = require('firebase-functions');
// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
const Json2csvParser = require('json2csv').Parser;
// CORS Express middleware to enable CORS Requests.
const cors = require('cors')({
  origin: true,
});

admin.initializeApp();

exports.getCsv = functions.https.onRequest((req, res) => {
  var rootRef = admin.database().ref('/');

  var jsonResponse = [];
  var userJson = {};
  var questionsArray = [];

  rootRef.child("users").once("value", userSnapshot => {
    userSnapshot.forEach(childSnapshot => {
      userJson[childSnapshot.key] = childSnapshot.val();
    });

    rootRef.child("answers").once("value", answersSnapshot => {
      answersSnapshot.forEach(childSnapshot => {
        addQuestion(childSnapshot.val(), questionsArray);
        if (userJson.hasOwnProperty(answer.id)) {
          addComment(childSnapshot.val(), userJson);
          addCheckBox(childSnapshot.val(), userJson);
          addOptionValue(childSnapshot.val(), userJson);
        }
      });

      for (id in userJson) {
        jsonResponse.push(userJson[id]);
      }

      var fields = createCsvFields(questionsArray);
      const json2csvParser = new Json2csvParser({ fields });
      const csv = json2csvParser.parse(jsonResponse);

      res.status(200)
        .set("Content-disposition", "attachment; filename=report.csv")
        .type('text/csv')
        .send(csv);
    }, error => {
      console.error(error);
      res.status(500).send('Internal Server Error');
    });
  }, error => {
    console.error(error);
    res.status(500).send('Internal Server Error');
  });
});

function addQuestion(answer, questionsArray) {
  questionsArray[answer.qNumber] = answer.question;
}

function addComment(answer, userJson) {
  console.log(answer.id);
  userJson[answer.id]['comment' + answer.qNumber] = answer.comments;
}

function addCheckBox(answer, userJson) {
  if (answer.hasOwnProperty('checkBox')) {
    var checkedBoxes = '';
    for (var i in answer.checkBox) {
      if (answer.checkBox[i].value === true) {
        checkedBoxes += answer.checkBox[i].key + ';';
      }
      userJson[answer.id]['option' + answer.qNumber] = checkedBoxes;
    }
  }
}

function addOptionValue(answer, userJson) {
  if (answer.hasOwnProperty('optionValue')) {
    userJson[answer.id]['option' + answer.qNumber] = answer.optionValue;
  }
}

function createCsvFields(questions) {
  var fields = ['name', 'age', 'gender', 'areaOfWork', 'employmentStatus', 'employmentYears'];
  for (var i = 1; i < questions.length; i++) {
    var questionHeader = {};
    questionHeader['label'] = questions[i];
    const questionProperty = 'comment' + i;
    questionHeader['value'] = (row, field) => row[questionProperty];
    fields.push(questionHeader);

    var optionHeader = {};
    optionHeader['label'] = 'option';
    const optionProperty = 'option' + i;
    optionHeader['value'] = (row, field) => row[optionProperty];
    fields.push(optionHeader);
  }
  return fields;
}

exports.submitForm = functions.https.onRequest((req, res) => {
  return cors(req, res, () => {
    req.accepts('application/json');
    var ref = admin.database().ref('/');
    var answers = req.body.indvQues;
    delete req.body.indvQues;
    var userRef = ref.child('users').push();
    userRef.set(req.body);

    var len, i;
    len = answers.length;
    for (i = 0; i < len; i++) {
      answers[i].id = userRef.key;
      ref.child('answers').push().set(answers[i]);
    }
    res.sendStatus(204);
  });
});
