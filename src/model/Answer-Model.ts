export class AnswerModel {
    //public or private property makes it easy 
    //to create and assign variables in a class.
    constructor(
        public id?: number,
        public name?: string,
        public age?: number,
        public gender?: string,
        public employmentStatus?: string,
        public employmentYears?: number,
        public areaOfWork?: string,
        public indvQues?: IndividualAnswer[]
    ) { }
}

export class IndividualAnswer {
    constructor(
        public qNumber: number,
        public question: string,
        public comments: string,
        public optionValue?: string,
        public checkBox?: CheckBoxModel[]
    ) { }

}


export class CheckBoxModel {
    constructor(public key: string,
        public value: boolean) { }


    getString(): string {
        return " [ " + this.key + " : " + this.value + " ] ";
    }
}



/**
 * Personal information: 
Name: [Optional] 
Age [ ] 
Gender: [ ] 
Education: [ ] 
Employment status: [ ] 
Years in employment: [ ]
Area of work: 
	Private sector 
	Non-Governmental    Organisation (“NGO”) 
	International donor organisations (e.g World Bank, Save the Children, UN etc) 
Self-employed 

 */