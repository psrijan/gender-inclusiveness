export class Questions {
    qno: number;
    question: string;
    qdesc?: string;
    optionType?: string;
    options?: string[];

    constructor(
        qno: number,
        question: string,
        qdesc: string,
        optionType: string,
        options: string[]
    ) {
        this.qno = qno;
        this.question = question;
        this.qdesc = qdesc;
        this.optionType = optionType;
        this.options = options;
    }
}



export let qList: Questions[] = [
    {
        qno: 1,
        question: "Interest - Are you interested in progressing / reaching senior leadership positions at your work?",
        qdesc: "Senior leadership role - Manager equivalent or above.",
        optionType: "radio",
        options: ["Very interested", "Interested", "Undecided", "Not Interested"]
    },
    {
        qno: 2,
        question: "Role models - Do you have female role models at work who you can look up to and aspire to be?",
        qdesc: "Role model is someone you look upto and aspire to be",
        optionType: "radio",
        options: ["Yes", "No", "Unsure"]
    },
    {
        qno: 3,
        question: "Career Path - Do you think there are clear opportunities for women to progress to senior leadership roles at your workplace",
        optionType: "radio",
        options: ["Yes", "No", "Unsure"]
    },
    {
        qno: 4,
        question: "Career break – Have you taken a career break? If so, what was the reason for it?",
        qdesc: "Definition: Any period in your career where you stopped working.",
        optionType: "radio",
        options: ["Yes", "No"]
    },
    {
        qno: 5,
        question: "Drivers for pursuing your career: What, in your opinion, drives you to achieve promotion or any other success at work? (Choose more than one if applicable)",
        optionType: "checkbox",
        options: ["Better financial stability (e.g. higher salary, better bonus)",
            "Pressure from family",
            "Personal satisfaction and fulfilment",
            "Desire for growth and continuous development",
            "Contribute to wider society, community and the country",
            "Other"
        ]
    },
    {
        qno: 6,
        question: "Challenges at workplace for women: What, in your opinion, are some of the key challenges women in Nepal face at workplace today?",
        optionType: "checkbox",
        options: ["Balancing personal and work life",
            "Lack of support from family",
            "Lack of support from workplace",
            "Other"]
    },
    {
        qno: 7,
        question: "Key hold-backs: What, in your opinion, are key social challenges for women in Nepal to rise to senior leadership positions at work? (Choose more than one if applicable)",
        optionType: "checkbox",
        options: ["Role of women at home and childcare",
            "Stereotypes and social conventions",
            "Lack of role models"]
    },
    {
        qno: 8,
        question: "Improvement: How do you think senior management, Board and Executive Committee members of your workplace can encourage and contribute to retain, develop women in senior leadership role?",
    },
    {
        qno: 9,
        question: "Importance: Do you see representation of women in senior leadership positions, at your / any other workplace in Nepal a challenge?",
        optionType: "radio",
        options: ["Yes", "No", "Unsure"]
    },
    {
        qno: 10,
        question: "Role: Who do you think is responsible for encouraging women to rise up to senior leadership ranks at work place? (Choose more than one if applicable)",
        optionType: "checkbox",
        options: ["Women at work",
            "Men at work",
            "Women and men at work",
            "Board, Executive Committee and current senior management",
            "No one"]

    },
    {
        qno: 11,
        question: "What, in you opinion, can you do, individually, the change the status quo of women in workplace in Nepal today?",
    }

];

/*

1. Interest – Are you interested in progressing / reaching senior leadership positions at your work?

Definitions: 

Senior leadership role – Manager equivalent or above.

Very interested 
Interested 
Undecided 
Not interested 


2. Role models – Do you have female role models at work who you can look up to and aspire to be? 

Definition - Role model is someone you look upto and aspire to be

Yes 
No 
Unsure 

3. Career paths – Do you think there are clear opportunities for women to progress to senior leadership roles at your workplace?

Yes 
No 
Unsure 

4. Career break – Have you taken a career break? If so, what was the reason for it? 

Definition: Any period in your career where you stopped working. 

Yes 
No 

If Yes, please provide reason for it below


5. Drivers for pursuing your career: What, in your opinion, drives you to achieve promotion or any other success at work? (Choose more than one if applicable) 

Better financial stability (e.g. higher salary, better bonus) 
Pressure from family 
Personal satisfaction and fulfilment 
Desire for growth and continuous development 
Contribute to wider society, community and the country
Other  


5. Challenges at workplace for women: What, in your opinion, are some of the key challenges women in Nepal face at workplace today? 

Balancing personal and work life 
Lack of support from family 
Lack of support from workplace 
Other 


6. Key hold-backs: What, in your opinion, are key social challenges for women in Nepal to rise to senior leadership positions at work? (Choose more than one if applicable)

Role of women at home and childcare  
Stereotypes and social conventions 
Lack of role models


7. Improvement: How do you think senior management, Board and Executive Committee members of your workplace can encourage and contribute to retain, develop women in senior leadership role? 


8. Importance: Do you see representation of women in senior leadership positions, at your / any other workplace in Nepal a challenge? 

Yes 
No 
Unsure


9. Role: Who do you think is responsible for encouraging women to rise up to senior leadership ranks at work place? (Choose more than one if applicable) 

Women at work 
Men at work 
Women and men at work 
Board, Executive Committee and current senior management 
No one   


10. What, in you opinion, can you do, individually, the change the status quo of women in workplace in Nepal today? 



-->
*/