// Basic model of a generic Page 
export class GenericModel {
    heading : string; 
    paragraphs : string[];
    topics : string[] 
}