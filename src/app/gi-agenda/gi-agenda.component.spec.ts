import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiAgendaComponent } from './gi-agenda.component';

describe('GiAgendaComponent', () => {
  let component: GiAgendaComponent;
  let fixture: ComponentFixture<GiAgendaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiAgendaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiAgendaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
