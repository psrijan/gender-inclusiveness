import { MaterialComponentModule } from './material-component.module';

describe('MaterialComponentModule', () => {
  let materialComponentModule: MaterialComponentModule;

  beforeEach(() => {
    materialComponentModule = new MaterialComponentModule();
  });

  it('should create an instance', () => {
    expect(materialComponentModule).toBeTruthy();
  });
});
