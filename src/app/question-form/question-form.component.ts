import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable } from "rxjs";
import { qList, Questions } from '../../model/Questions-List-Model';
import { CheckBoxModel, AnswerModel, IndividualAnswer } from '../../model/Answer-Model';

@Component({
  selector: 'app-question-form',
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.scss']
})

export class QuestionFormComponent implements OnInit {

  questions: Questions[] = qList;
  answers: AnswerModel = new AnswerModel();
  submitted: boolean = false;
  // favoriteColors : string ="asd";
  // colorValue: string="Red";

  constructor(private http: HttpClient) { }

  endpoint = 'https://us-central1-gender-inclusiveness.cloudfunctions.net/submitForm';

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  onSubmit() {
    this.http.post(this.endpoint, JSON.stringify(this.answers), this.httpOptions)
      .subscribe(
        data => {
          this.submitted = true;
          window.scroll(0,0);
        },
        error => {
          console.log("Error submitting form ", error);
        }
      );
  }

  ngOnInit() {
    let size = this.questions.length;
    this.answers.indvQues = [];

    for (let i = 0; i < size; i++) {
      // console.log("ANS MODEL: " + i);
      let checkBoxOptionArr = [];

      let ansModel = new IndividualAnswer(i + 1, this.questions[i].question, "");

      if (this.questions[i].optionType == "checkbox") {
        let checkBoxOption = this.questions[i].options;
        for (let j = 0; j < checkBoxOption.length; j++) {
          let checkBoxModel = new CheckBoxModel(checkBoxOption[j], false);
          checkBoxOptionArr.push(checkBoxModel);
        }
        ansModel.checkBox = checkBoxOptionArr;
      }

      if (this.questions[i].optionType == "radio") {
        ansModel.optionValue = "";
      }

      this.answers.indvQues[i] = ansModel;
    }
    // console.log("Size: " + this.answers.indvQues.length);
  }

  displayFormData(): string {
    let resStr = "id: {id} \n" +
      "name: {name} \n" +
      "age: {age} \n" +
      "gender: {gender} \n" +
      "employmentStatus: {employmentStatus} \n" +
      "employmentYears: {employmentYears} \n" +
      "areaOfWork: {areaOfWork} \n";
    resStr = resStr.replace("{name}", this.answers.name);
    resStr = resStr.replace("{age}", this.answers.age + "");
    resStr = resStr.replace("{gender}", this.answers.gender);
    resStr = resStr.replace("{employmentStatus}", this.answers.employmentStatus);
    resStr = resStr.replace("{employmentYears}", this.answers.employmentYears + "");
    resStr = resStr.replace("{areaOfWork}", this.answers.areaOfWork);
    return resStr;
  }

  displayQuestionData() {
    let allAnswers = this.answers.indvQues;
    let retStr = "";
    for (let i = 0; i < allAnswers.length; i++) {
      retStr += "" + allAnswers[i].qNumber;
    }
    alert(allAnswers.length + " -  " + retStr);
  }

  printQuestionData() {
    let allAnswers = this.answers.indvQues;
    let retStr = "";

    for (let i = 0; i < allAnswers.length; i++) {
      retStr += " [ " + allAnswers[i].qNumber;
      retStr += " Option Value: " + allAnswers[i].optionValue + " ";
      retStr += " " + allAnswers[i].comments + " ] ";
    }

    return retStr;
  }


}

/*
   constructor(
        public id?: number, 
        public name?: string,
        public age? : number, 
        public gender? : string, 
        public employmentStatus? : string, 
        public employmentYears? : number, 
        public areaOfWork? : string,
        public indvQues? : IndividualAnswer[]
    ) {}
*/

