import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmeEmbraceTdyComponent } from './rme-embrace-tdy.component';

describe('RmeEmbraceTdyComponent', () => {
  let component: RmeEmbraceTdyComponent;
  let fixture: ComponentFixture<RmeEmbraceTdyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmeEmbraceTdyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmeEmbraceTdyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
