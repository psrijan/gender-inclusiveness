import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-rme-embrace-tdy',
  templateUrl: './rme-embrace-tdy.component.html',
  styleUrls: ['./rme-embrace-tdy.component.css']
})
export class RmeEmbraceTdyComponent implements OnInit {

  chartDemographics = [];
  chartRegion = [];
  constructor() { }

  ngOnInit() {
    var configDemographics = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            17, 83
          ],
          backgroundColor: [
            '#f4511e',
            '#039be5'
          ]
        }],
        labels: [
          'White Men',
          'Women & Ethnic Minority Individuals'
        ]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'By Demographics',
          fontSize: 24
        }
      }
    };
    var configRegion = {
      type: 'pie',
      data: {
        datasets: [{
          data: [
            10, 13, 53, 4, 2, 5, 12
          ],
          backgroundColor: [
            '#e53935',
            '#8e24aa',
            '#3949ab',
            '#039be5',
            '#00897b',
            '#7cb342',
            '#fdd835'
          ]
        }],
        labels: [
          'Western Europe',
          'Central/Eastern Europe',
          'Asia - Pacific',
          'MENA',
          'Africa',
          'South America',
          'North America'
        ]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'By Region',
          fontSize: 24
        }
      }
    };
    this.chartDemographics = new Chart('canvasDemographics', configDemographics);
    this.chartRegion = new Chart('canvasRegion', configRegion);
  }

}
