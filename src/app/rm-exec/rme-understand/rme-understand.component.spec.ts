import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmeUnderstandComponent } from './rme-understand.component';

describe('RmeUnderstandComponent', () => {
  let component: RmeUnderstandComponent;
  let fixture: ComponentFixture<RmeUnderstandComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmeUnderstandComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmeUnderstandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
