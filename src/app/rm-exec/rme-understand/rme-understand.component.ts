import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-rme-understand',
  templateUrl: './rme-understand.component.html',
  styleUrls: ['./rme-understand.component.scss']
})
export class RmeUnderstandComponent implements OnInit {

  chart = [];
  constructor() { }

  ngOnInit() {
    var chartConfig = {
      type: 'bar',
      data: {
        labels: [['Because it benefits', 'my protege'],
        ['Because it benefits', 'me']],
        datasets: [{
          label: 'Male Sponsors',
          data: [62, 77],
          backgroundColor: [
            '#039be5',
            '#039be5',
            '#039be5'
          ],
          borderWidth: 0
        }, {
          label: 'Female Sponsors',
          data: [74, 60],
          backgroundColor: [
            '#006db3',
            '#006db3',
            '#006db3'
          ],
          borderWidth: 0
        }]
      },
      options: {
        responsive: true,
        title: {
          display: true,
          text: 'Why do you sponsor?',
          fontSize: 24
        },
        scales: {
          yAxes: [{
            type: 'linear',
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    };
    this.chart = new Chart('canvas', chartConfig);
  }

}
