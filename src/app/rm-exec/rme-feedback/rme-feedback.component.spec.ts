import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmeFeedbackComponent } from './rme-feedback.component';

describe('RmeFeedbackComponent', () => {
  let component: RmeFeedbackComponent;
  let fixture: ComponentFixture<RmeFeedbackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmeFeedbackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmeFeedbackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
