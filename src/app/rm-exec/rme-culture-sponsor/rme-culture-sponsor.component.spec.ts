import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmeCultureSponsorComponent } from './rme-culture-sponsor.component';

describe('RmeCultureSponsorComponent', () => {
  let component: RmeCultureSponsorComponent;
  let fixture: ComponentFixture<RmeCultureSponsorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmeCultureSponsorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmeCultureSponsorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
