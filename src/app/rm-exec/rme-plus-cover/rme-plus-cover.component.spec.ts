import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmePlusCoverComponent } from './rme-plus-cover.component';

describe('RmePlusCoverComponent', () => {
  let component: RmePlusCoverComponent;
  let fixture: ComponentFixture<RmePlusCoverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmePlusCoverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmePlusCoverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
