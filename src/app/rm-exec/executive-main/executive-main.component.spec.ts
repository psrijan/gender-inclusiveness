import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExecutiveMainComponent } from './executive-main.component';

describe('ExecutiveMainComponent', () => {
  let component: ExecutiveMainComponent;
  let fixture: ComponentFixture<ExecutiveMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExecutiveMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExecutiveMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
