import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RmeMenteeProtegComponent } from './rme-mentee-proteg/rme-mentee-proteg.component';

import { RmeUnderstandComponent } from './rme-understand/rme-understand.component';
import { RmeEmbraceTdyComponent } from './rme-embrace-tdy/rme-embrace-tdy.component';
import { RmeComeThruComponent } from './rme-come-thru/rme-come-thru.component';
import { RoadmapExecutiveComponent } from './roadmap-executive/roadmap-executive.component'
import { ExecutiveMainComponent } from './executive-main/executive-main.component';
import { RouterModule , Routes } from '@angular/router';
import { RmePlusCoverComponent } from './rme-plus-cover/rme-plus-cover.component';
import { RmeFeedbackComponent } from './rme-feedback/rme-feedback.component';
import { RmeCultureSponsorComponent } from './rme-culture-sponsor/rme-culture-sponsor.component';
import { RmlExudeExecComponent } from '../rm-leader/rml-exude-exec/rml-exude-exec.component';

const execRoutes : Routes = [
  {path: 'roadmap-for-sponsoring-executives/step1' , component : RmeUnderstandComponent},
  {path: 'roadmap-for-sponsoring-executives/step2' , component : RmeEmbraceTdyComponent},
  {path: 'roadmap-for-sponsoring-executives/step3' , component : RmeMenteeProtegComponent},
  {path: 'roadmap-for-sponsoring-executives/step4' , component : RmeComeThruComponent},
  {path: 'roadmap-for-sponsoring-executives/step5' , component : RmePlusCoverComponent},
  {path: 'roadmap-for-sponsoring-executives/step6' , component : RmeFeedbackComponent},
  {path: 'roadmap-for-sponsoring-executives/step7' , component : RmeCultureSponsorComponent},
  {path: 'roadmap-for-sponsoring-executives/step8', component: RmlExudeExecComponent}
];


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild( execRoutes),
  ],
  declarations: [
    RmeCultureSponsorComponent,
    RmeFeedbackComponent,
    RmePlusCoverComponent,
    RmeUnderstandComponent,
    RmeUnderstandComponent,
    RmeEmbraceTdyComponent,
    RmeComeThruComponent,
    RoadmapExecutiveComponent,
    ExecutiveMainComponent,
    RmeMenteeProtegComponent
  ],
  exports : [RouterModule, 
    ExecutiveMainComponent
  ]
})
export class RmExecModule { }

