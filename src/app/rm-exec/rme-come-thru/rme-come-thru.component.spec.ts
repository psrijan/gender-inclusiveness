import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmeComeThruComponent } from './rme-come-thru.component';

describe('RmeComeThruComponent', () => {
  let component: RmeComeThruComponent;
  let fixture: ComponentFixture<RmeComeThruComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmeComeThruComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmeComeThruComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
