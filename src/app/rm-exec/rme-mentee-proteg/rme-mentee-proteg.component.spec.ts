import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmeMenteeProtegComponent } from './rme-mentee-proteg.component';

describe('RmeMenteeProtegComponent', () => {
  let component: RmeMenteeProtegComponent;
  let fixture: ComponentFixture<RmeMenteeProtegComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmeMenteeProtegComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmeMenteeProtegComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
