import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-roadmap-executive',
  templateUrl: './roadmap-executive.component.html',
  styleUrls: ['./roadmap-executive.component.scss']
})
export class RoadmapExecutiveComponent implements OnInit {

  steps : Step[] = [
    { stepNo: 1 , stepName: "Understand what’s in it for you"},
    { stepNo: 2 , stepName: "Embrace today’s talent imperatives"},
    { stepNo: 3 , stepName: "Differentiate between mentees and protégés"},
    { stepNo: 4 , stepName: "Come through on two essential fronts"},
    { stepNo: 5 , stepName: "PLUS, provide air cover"},
    { stepNo: 6 , stepName: "Give critical feedback on hard issue"},
    { stepNo: 7 , stepName: "Help create a culture of sponsorship at your organization"}

  ];

  constructor() { }

  ngOnInit() {
  }

}

class Step {
  constructor(public stepNo : number , 
    public stepName : string){}
}