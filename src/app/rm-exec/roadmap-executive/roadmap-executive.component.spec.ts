import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadmapExecutiveComponent } from './roadmap-executive.component';

describe('RoadmapExecutiveComponent', () => {
  let component: RoadmapExecutiveComponent;
  let fixture: ComponentFixture<RoadmapExecutiveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoadmapExecutiveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadmapExecutiveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
