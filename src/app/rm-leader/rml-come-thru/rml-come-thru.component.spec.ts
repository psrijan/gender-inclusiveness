import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmlComeThruComponent } from './rml-come-thru.component';

describe('RmlComeThruComponent', () => {
  let component: RmlComeThruComponent;
  let fixture: ComponentFixture<RmlComeThruComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmlComeThruComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmlComeThruComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
