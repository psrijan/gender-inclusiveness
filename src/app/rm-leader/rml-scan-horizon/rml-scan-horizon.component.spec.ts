import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmlScanHorizonComponent } from './rml-scan-horizon.component';

describe('RmlScanHorizonComponent', () => {
  let component: RmlScanHorizonComponent;
  let fixture: ComponentFixture<RmlScanHorizonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmlScanHorizonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmlScanHorizonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
