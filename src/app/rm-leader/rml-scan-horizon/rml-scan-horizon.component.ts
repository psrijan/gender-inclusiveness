import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-rml-scan-horizon',
  templateUrl: './rml-scan-horizon.component.html',
  styleUrls: ['./rml-scan-horizon.component.scss']
})
export class RmlScanHorizonComponent implements OnInit {

  chart = [];
  constructor() { }

  ngOnInit() {
    var chartConfig = {
      type: 'bar',
      data: {
        labels: [['"Classic":', 'Old School/Hierarchical', '(Values Loyalty)'],
        ['"Competitive:', 'Hard edged/Hard-driving', '(Values bottom-line results)'],
        ['"Collaborative":', 'Inclusive/people focused', '(Values a speak-up culture)'],
        ['"Charismatic":', 'Inspirational/creative', '(Valuesbig picture)']],
        datasets: [{
          label: 'Composition of leadership',
          data: [45, 20, 28, 6],
          backgroundColor: [
            '#039be5',
            '#039be5',
            '#039be5',
            '#039be5',
            '#039be5'
          ],
          borderWidth: 0
        }, {
          label: 'Preference of female protegees',
          data: [17, 16, 42, 24],
          backgroundColor: [
            '#006db3',
            '#006db3',
            '#006db3',
            '#006db3',
            '#006db3'
          ],
          borderWidth: 0
        }]
      },
      options: {
        responsive: true,
        scales: {
          yAxes: [{
            ticks: {
              beginAtZero: true
            }
          }]
        }
      }
    };
    this.chart = new Chart('canvas', chartConfig);
  }

}
