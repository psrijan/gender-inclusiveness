import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlusDeliverComponent } from './plus-deliver.component';

describe('PlusDeliverComponent', () => {
  let component: PlusDeliverComponent;
  let fixture: ComponentFixture<PlusDeliverComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlusDeliverComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlusDeliverComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
