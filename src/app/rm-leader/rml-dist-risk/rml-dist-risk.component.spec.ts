import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmlDistRiskComponent } from './rml-dist-risk.component';

describe('RmlDistRiskComponent', () => {
  let component: RmlDistRiskComponent;
  let fixture: ComponentFixture<RmlDistRiskComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmlDistRiskComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmlDistRiskComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
