import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmlNailTacticsComponent } from './rml-nail-tactics.component';

describe('RmlNailTacticsComponent', () => {
  let component: RmlNailTacticsComponent;
  let fixture: ComponentFixture<RmlNailTacticsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmlNailTacticsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmlNailTacticsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
