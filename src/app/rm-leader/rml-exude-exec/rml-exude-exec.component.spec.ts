import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmlExudeExecComponent } from './rml-exude-exec.component';

describe('RmlExudeExecComponent', () => {
  let component: RmlExudeExecComponent;
  let fixture: ComponentFixture<RmlExudeExecComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmlExudeExecComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmlExudeExecComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
