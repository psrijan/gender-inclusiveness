import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadershipMainComponent } from './leadership-main.component';

describe('LeadershipMainComponent', () => {
  let component: LeadershipMainComponent;
  let fixture: ComponentFixture<LeadershipMainComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadershipMainComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadershipMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
