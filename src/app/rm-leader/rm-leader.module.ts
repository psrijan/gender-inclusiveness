import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeadershipMainComponent } from './leadership-main/leadership-main.component';
import { RoadmapLeadershipComponent } from './roadmap-leadership/roadmap-leadership.component';
import { RmlEmbraceDreamComponent } from './rml-embrace-dream/rml-embrace-dream.component';
import { RmlScanHorizonComponent } from './rml-scan-horizon/rml-scan-horizon.component';
import { RmlDistRiskComponent } from './rml-dist-risk/rml-dist-risk.component';
import { RmlComeThruComponent } from './rml-come-thru/rml-come-thru.component';
import { PlusDeliverComponent } from './plus-deliver/plus-deliver.component';
import { RmlExudeExecComponent } from './rml-exude-exec/rml-exude-exec.component';
import { RmlNailTacticsComponent } from './rml-nail-tactics/rml-nail-tactics.component';

import { Routes, RouterModule } from '@angular/router';

const leaderRoutes: Routes = [
  { path: 'roadmap-for-women/step1', component: RmlEmbraceDreamComponent },
  { path: 'roadmap-for-women/step2', component: RmlScanHorizonComponent },
  { path: 'roadmap-for-women/step3', component: RmlDistRiskComponent },
  { path: 'roadmap-for-women/step4', component: RmlComeThruComponent },
  { path: 'roadmap-for-women/step5', component: PlusDeliverComponent },
  { path: 'roadmap-for-women/step6', component: RmlExudeExecComponent },
  { path: 'roadmap-for-women/step7', component: RmlNailTacticsComponent },
];

/**
 *  { stepNo: 1, stepName:	"Embrace your dream"},
    { stepNo: 2, stepName:    "Scan the horizon for powerful sponsors"},
    { stepNo: 3, stepName:    "Distribute your risk"},
    { stepNo: 4, stepName:    "Come through on two obvious fronts" },
    { stepNo: 5, stepName:    "PLUS, deliver a distinct personal brand"},
    { stepNo: 6, stepName:    "Exude Executive Presence"},
    { stepNo: 7, stepName:    "Nail the tacktics"}
 */

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(leaderRoutes)
  ],
  declarations: [LeadershipMainComponent,
    RmlEmbraceDreamComponent,
    RmlScanHorizonComponent,
    RmlDistRiskComponent,
    RmlComeThruComponent,
    PlusDeliverComponent,
    RmlExudeExecComponent,
    RmlNailTacticsComponent,
    RoadmapLeadershipComponent
  ],
  exports: [
    RouterModule,
    LeadershipMainComponent]
})
export class RMLeaderModule { }

