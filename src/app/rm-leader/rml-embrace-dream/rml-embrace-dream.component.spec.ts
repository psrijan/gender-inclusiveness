import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RmlEmbraceDreamComponent } from './rml-embrace-dream.component';

describe('RmlEmbraceDreamComponent', () => {
  let component: RmlEmbraceDreamComponent;
  let fixture: ComponentFixture<RmlEmbraceDreamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RmlEmbraceDreamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RmlEmbraceDreamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
