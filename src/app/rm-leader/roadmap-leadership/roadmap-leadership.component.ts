import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-roadmap-leadership',
  templateUrl: './roadmap-leadership.component.html',
  styleUrls: ['./roadmap-leadership.component.scss']
})
export class RoadmapLeadershipComponent implements OnInit {

  steps: Step[] = [
    { stepNo: 1, stepName: "Embrace your dream" },
    { stepNo: 2, stepName: "Scan the horizon for powerful sponsors" },
    { stepNo: 3, stepName: "Distribute your risk" },
    { stepNo: 4, stepName: "Come through on two obvious fronts" },
    { stepNo: 5, stepName: "PLUS, deliver a distinct personal brand" },
    { stepNo: 6, stepName: "Exude Executive Presence" },
    { stepNo: 7, stepName: "Nail the Tactics" }
  ];

  constructor() { }

  ngOnInit() { }

}

class Step {
  constructor(public stepNo: number,
    public stepName: string) { }
}
