import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RoadmapLeadershipComponent } from './roadmap-leadership.component';

describe('RoadmapLeadershipComponent', () => {
  let component: RoadmapLeadershipComponent;
  let fixture: ComponentFixture<RoadmapLeadershipComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RoadmapLeadershipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RoadmapLeadershipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
