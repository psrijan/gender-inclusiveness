import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { RMLeaderModule } from './rm-leader/rm-leader.module';
import { RmExecModule } from './rm-exec/rm-exec.module';
import { QuestionFormComponent } from './question-form/question-form.component';
import { LeadershipMainComponent } from './rm-leader/leadership-main/leadership-main.component';
import { ExecutiveMainComponent } from './rm-exec/executive-main/executive-main.component';

import { MaterialComponentModule } from './material-component/material-component.module';
import { GiAgendaComponent } from './gi-agenda/gi-agenda.component';

const appRoutes: Routes = [
  { path: 'forms', component: QuestionFormComponent },
  { path: 'landing', component: LandingPageComponent },
  { path: "roadmap-for-women", component: LeadershipMainComponent },
  { path: "roadmap-for-sponsoring-executives", component: ExecutiveMainComponent },
  { path: "agenda", component: GiAgendaComponent },
  { path: '**', component: LandingPageComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    QuestionFormComponent,
    GiAgendaComponent,
  ],

  imports: [
    NgbModule,
    HttpClientModule,
    MaterialComponentModule,
    BrowserModule,
    FormsModule,
    CommonModule,
    RMLeaderModule,
    RmExecModule,
    RouterModule.forRoot(
      appRoutes
    ),

  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule { }
